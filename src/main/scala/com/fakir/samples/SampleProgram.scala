package com.formation.spark

import com.fakir.samples.models._
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.SparkSession

import scala.io.Source
object SampleProgram {

  def main(args: Array[String]): Unit = {
    /*Logger.getLogger("org").setLevel(Level.OFF)


    val sparkConf = new SparkConf().setAppName("My First Program in Spark!")
      .setMaster("local[2]")
    val sparkContext = new SparkContext(sparkConf)

    val rddFromList = sparkContext.parallelize(Seq("this", "is", "an", "rdd", "from", "a", "Sequence"))
    rddFromList.map(_.toUpperCase).foreach(println)


    //If you want to start working with Spark 2, go through a SparkSession!
    val sparkSession = SparkSession.builder().getOrCreate()*/
    //print(matchBrand("Apple"))
    //print(wichAnimal(Cat("minou", 5, 10)))

    //print(wichGender(Alien("Marie")))

    //val sequence = Seq("Hyperledger", "Corda R3", "Blockchain", "Toto")
    //sequence.map(majuscule).foreach(println)
    //for(s <- sequence) println(s)
    //val numbers = Seq(100, 200, 350, 700, 15000, 200000)

    //val n = 600
    //print(separate(sequence,";"))
    //print(numbers.foldLeft(0){(acc,element) => closestNumber(acc,element,n)})
    //filterLinesFile("/Users/benjamindumont1/Documents/Developpement/Workspace-Java/Scala/scala-spark-boilerplate/src/main/scala/com/fakir/samples/fileExample.csv")
    //filterLinesFile(getRidOfHeader(linesFromFile("/Users/benjamindumont1/Documents/Developpement/Workspace-Java/Scala/scala-spark-boilerplate/src/main/scala/com/fakir/samples/fileExample.csv"))).foreach(println)
    println(converter(140,"m",unitConverter))
  }

  //def `hnn



  def wichGender(Gender: Any) : String = {
    Gender match {
      case Alien(_) => "JGAHI IUHGZRGREZOPUA Alien"
      case Man(_) => "Je suis un homme"
      case Woman(name) => s"Je suis une femme et je m'appelle $name"
    }
  }

  def separate (tab : Seq[String], replaceIndentifier : String) : String = {
    tab.mkString(replaceIndentifier)
  }

  def closestNumber(i: Int, j: Int, k: Int) : Int = {
    if(Math.abs(k - i) > Math.abs(k - j)){
      j
    } else{
      i
    }
  }

  def majuscule(s: String) : String = {
    s.toUpperCase
  }

  def wichAnimal(animal: Animal) : String = {
    animal match {
      case Cat(name, age, _) if name == "minou" && age > 3 => s"your cat's name is $name and aged of $age years"
      case Dog(name, age, _) if name == "Dogo" => s"your dog's name is $name "

    }
  }

  def matchBrand(osName: String): String = osName match {
    case "Apple" => "Whaou tu as un mac bg woullah"
    case "Microsoft" => "sale pauvre barra d'içi"
    case _ => "homme de cromagnon"

  }

  def linesFromFile(path: String) : Array[String] = {
    Source.fromFile(path).getLines.toArray
  }

  def getRidOfHeader(array: Array[String]) : Array[String] = {
    val header = array.head
    array.filter(line => line != header)
  }

  def filterLinesFile(array: Array[String]) : Array[String] = {
    //lines.foreach(println)
    //val linesWithoutHead = array.filter(line => line != header)
    //linesWithoutHead.foreach(println)
    val dataFiltered = array.filter(line => line.split(", ")(2).toInt > 2000)
    val finalData = dataFiltered.filter(line => line.split(", ")(3).toInt > 1)
    finalData
  }

  def converter(value : Int, unit : String, f: (Int, String) => Float) : Float = {
    f(value, unit)
  }

  def unitConverter(value: Int, unit: String): Float = unit match {
    case "m" => value.toFloat / 100
    case "mm" => value * 10
    case _ => -1

  }

  def studentMatchSubject() : Boolean = {
    true
  }

  def hasTakenSubject(nomEleve: String, matiere: String) : Option[String] = {
    if(studentMatchSubject)
      Some(nomEleve)
    else
      None
  }

  def noteEleve(matiere: String, note: Double, nomEleve: Option[String]) : Double = {
    if(nomEleve.isDefined) {
      note
    }
    else
      0.0
  }
}

